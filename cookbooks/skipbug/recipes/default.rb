# Update the repository and install all upgrades
execute "update apt" do
    command "sudo apt-get update && sudo apt-get upgrade -y"
end

package 'apache2'
package 'php5'
package 'libapache2-mod-php5'
package 'mysql-server'
package 'libapache2-mod-auth-mysql'
package 'php5-mysql'

execute 'create app db' do
    command %{mysql -u root -e 'CREATE DATABASE IF NOT EXISTS arta'}
end

execute 'soft link' do
    command %{ln -s /vagrant/src/PHP /var/www/arta}
    command %{/etc/init.d/apache2 restart}
end

execute 'change apache vhost' do
    command %{cp /vagrant/src/DOC/default /etc/apache2/sites-available/}
    command %{/etc/init.d/apache2 restart}
end

execute 'enable mod_rewrite' do
    command %{a2enmod rewrite}
    command %{/etc/init.d/apache2 restart}
end

execute 'install sql' do
    command %{cd /vagrant/src/DOC && sh install.sh}
end

