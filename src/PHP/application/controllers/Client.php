<?php

class Client_controller extends Controller {

    function index() {
        $post = $_POST;
        $this->load->model("Client");
        $response = array();
        $response["mode"] = "loginform";
        $client = $this->Client->getCurrentClient();
        $tmpl = 'login';
        if ($this->isTimedOut()) {
            $message = "Jūs atlikote per daug užklausų. Prašome palaukti ir pabandyti vėliau";
        } else if ($client) {
            header('Location: /');
        }
        else if (isset($post['task']) && ($post['task'] == "login")) {
            if (NULL != ($client = $this->Client->authenticate($post['email'], $post['pass']))) {
                $this->session->set('client', $client);
                $this->session->set('attempts');
                $this->Client->setLastVisit($client['clientid']);
                header('Location: /');
            } else {
                if (NULL == ($attempt = $this->session->get('attempts'))) {
                    $attempt = 0;
                }

                $attempt++;

                $this->session->set('attempts', $attempt); 
                $this->session->set('lastrequest', time()); 
                $message = "Neteisingi duomenys";
            }

        }
        if (isset($message)) {
            $response['message'] = $message;
        }
        $this->template->load($tmpl, "Client/Auth", $response);
    }

    private function isTimedOut()
    {
        if ($this->session->get('lastrequest') + 5 < time()) {  // last attempt was long long time ago - everything is OK
            $this->session->set('attempts');
            return false;
        }
        if ($this->session->get('timeoutstart') + 30 > time()) {  //timedout for 30 s
            return true;
        }
        if ($this->session->get('attempts') >= 5) {
            $this->session->set('timeoutstart', time());
            $this->session->set('attempts');
            return true;
        }
        $this->session->set('timeoutstart');
        return false;
    }

    function register() {
        $this->load->model("Client");
        $post = $_POST;
        $response = array();
        $response["mode"] = "registerform";
        $client = $this->session->get('client');

        if (isset($client)) {
            header('Location: /');
        } else if (isset($post['task']) && ($post['task'] == "register")) {
            if ((!isset($post['email']))       || (empty($post['email']))       ||
                (!isset($post['pass']))        || (empty($post['pass']))        ||
                (!isset($post['pass-repeat'])) || (empty($post['pass-repeat']))
               ) 
            {
                $message = "Ne visi laukai buvo užpildyti";
            } else if ($post['pass'] != $post['pass-repeat']) {
                $message = "Neteisingai pakartojot slaptažodį";
            } else {
                $clientData = array("email" => $post["email"],
                                    "pass"  => $post["pass"]);
                if ($this->Client->getClientEmail($post['email'])) {
                    $message = "Toks emailas jau užregistruotas";
                } else {
                    $this->Client->saveclient($clientData);
                    if (NULL != ($client = $this->Client->authenticate($post['email'], $post['pass']))) {
                        $this->session->set('client', $client);
                        $this->Client->setLastVisit($client['clientid']);
                        header('Location: /');
                    }
                }
            }
        }
        $response['message'] = (isset($message)) ? $message : '';
        $this->template->load("login", "Client/Auth", $response);
    }

    function logout() {
        $this->session->destroy();
        header('Location: /');
    }


    function personalInfo() {
        $this->load->model("Client");

        if (null == ($client = $this->Client->getCurrentClient())) {
            header('Location: /');
        } else {
            $post = $_POST;
            $response = array(); 
            if(isset($post['update'])) {
                if (empty($post['pass']) || empty($post['pass-2']) || ($post['pass'] != $post['pass-2'])) {
                    $message = "Klaida suvedant duomenis";
                } else {
                   $this->Client->saveClient($post, $client['clientid']); 
                   $message = "Sėkmingai atnaujinta";
                }
                $response['message'] = $message;
            }
        }
        $this->template->load('default', "Client/PersonalInformation", $response);
    }
}
