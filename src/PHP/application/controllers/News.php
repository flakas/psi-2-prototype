<?php

class News_controller extends Controller {

    function __construct() {
        $this->load->model("Client");
        $this->currentClient = $this->Client->getCurrentClient();
        if (!$this->currentClient) {
            header('Location: /Client');
        }
    }

    function index() {
        $this->load->model("Source");
        $this->load->model("News");
        $this->load->model('Filter');

        $filters = $this->Filter->getClientFiltersConditions($this->currentClient);
        $news = $this->News->getAllFilteredClientNews($this->currentClient, $filters);

        $popular = $this->Source->getPopularSources($this->currentClient, 5);
        $this->template->load("default", "News/Reader", array('news' => $news, 'popular' => $popular));
    }

    function sources() {
        $this->load->model("Source");
        $sources = $this->Source->getClientSources($this->currentClient, 0, 0);
        $this->template->load("default", "News/Sources", array('sources' => $sources));
    }

    function addSource() {
        $this->load->model("Source");
        if (isset($_POST['submit'])) {
            $response = array();
            $url = trim($_POST['url']);
            if (!empty($url)) {
                $response['success'] = $this->Source->addClientSource($this->currentClient, $url);
            } else {
                $response['success'] = false;
                $response['message'] = 'Nurodykite teisingą adresą';
            }
            $this->template->load("default", "News/AddSource", array('_POST' => $_POST, 'response' => $response));
        } else {
            $this->template->load("default", "News/AddSource");
        }
    }

    function removeSource() {
        $this->load->model("Source");
        if (isset($_POST['submit'])){
            $response = array();
            $id = (int) trim($_POST['source_id']);
            if (!empty($id)) {
                $response['success'] = $this->Source->removeClientSource($this->currentClient, $id);
            } else {
                $response['success'] = false;
                $response['message'] = 'Nurodykite teisingą šaltinį';
            }
            $this->template->load("default", "News/RemoveSource", array('_POST' => $_POST, 'response' => $response));
        } else {
            $this->template->load("default", "News/RemoveSource", array('sources' => $this->Source->getClientSources($this->currentClient)));
        }
    }

    function filters() {
        $this->load->model("Filter");
        $filters = $this->Filter->getClientFiltersList($this->currentClient);
        $this->template->load("default", "News/Filters", array('filters' => $filters));
    }

    function addFilter() {
        $this->load->model("Filter");
        $this->load->model("Source");
        if (isset($_POST['submit'])) {
            $response = array();
            $source = (int) $_POST['source'];
            $filter_type = (int) $_POST['filter_type'];
            $filter_term = $_POST['term'];
            if (!$source || !$filter_type || empty($filter_term)) {
                $response['success'] = false;
                $response['message'] = 'Nurodykite visus duomenis';
            } else {
                $filters = array();
                $filters[] = array(
                    'type' => $filter_type,
                    'term' => $filter_term
                );
                $response['success'] = $this->Filter->addClientFilter($this->currentClient, $source, $filters);
            }
            $this->template->load("default", "News/AddFilter", array('response' => $response));
        } else {
            $availableSources = $this->Source->getAvailableClientSources($this->currentClient);
            $this->template->load("default", "News/AddFilter", array('sources' => $availableSources));
        }
    }

    function removeFilter() {
        $this->load->model("Filter");
        if (isset($_POST['submit'])) {
            $response = array();
            $id = (int) trim($_POST['filter_id']);
            if (!empty($id)) {
                $response['success'] = $this->Filter->removeClientFilter($id);
            } else {
                $response['success'] = false;
                $response['message'] = 'Nurodykite teisingą filtrą';
            }
            $this->template->load("default", "News/RemoveFilter", array('_POST' => $_POST, 'response' => $response));
        } else {
            $this->template->load("default", "News/RemoveFilter", array('filters' => $this->Filter->getClientFiltersList($this->currentClient)));
        }
    }
}
