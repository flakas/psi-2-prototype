<?php

class Update_controller extends Controller {
    public function index() {
        $this->load->model('Source');
        $this->load->model('News');
        $this->load->library('Simplepie');

        $xmlparser = $this->libraries->Simplepie;
        $xmlparser->set_timeout(30);
        $xmlparser->enable_order_by_date();
        $xmlparser->enable_cache(false);
        $sources = $this->Source->getAllSources();

        $itemsCounter = 0;
        foreach ($sources as $source) {
            $xmlparser->set_feed_url($source['url']);
            $xmlparser->init();
            $xmlparser->handle_content_type();

            $start = 0;
            $step = 100;
            $limit = $step;

            $news = array();

            while ($start < $xmlparser->get_item_quantity()) {
                foreach ($xmlparser->get_items($start, $limit) as $item) {
                    if ($this->News->getNewsByGuidAndSource($item->get_id(), $source['id']) == NULL) {
                        $newsItem           = new stdclass();
                        $newsItem->title    = trim($item->get_title());
                        $newsItem->text     = htmlspecialchars_decode(trim($item->get_description()));
                        $newsItem->date     = ($item->get_date('U')) ? $item->get_date('Y-m-d H:i:s') : date('Y-m-d H:i:s');
                        $newsItem->guid     = $item->get_id();
                        $newsItem->sourceid = $source['id'];
                        $newsItem->full_url = $item->get_link();

                        $news[] = $newsItem;

                        $itemsCounter++;
                    }
                }
                if (count($news)) {
                    $this->News->saveNews($news);
                }

                $news = array();

                $start += $step;
                $limit += $step;
            }

            $this->Source->setSourceLastCheck($source['id']);
            if ($itemsCounter == 0) {
                $this->Source->increaseInactivityTimes($source['id']);
            }


            if ($source['title'] == NULL) {
                $this->Source->setSourceTitle($source['id'], $xmlparser->get_title());
            }
        }
        echo "Update was succesfully performed. ".$itemsCounter." new items added";
    }
}
