<?php

class Template {

    private $core;

    function __construct() {
        $this->core = get_instance();
    }

    function load($tpl, $body = NULL, $data = NULL) {
        if ($body != NULL) {
            $body = $this->core->load->view($body, $data, true);
            if (is_null($data)) {
                $data = array('body' => $body);
            } else if (is_array($data)) {
                $data['body'] = $body;
            }
        }
        $this->core->load->view('Templates/'.$tpl, $data);
    }

}
