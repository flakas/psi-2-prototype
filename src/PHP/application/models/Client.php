<?php

class Client extends Model {

    function getClient($clientid) {
        $sql = "SELECT * FROM clients WHERE id='%d'";
        $result = $this->db->querySafe($sql, array((int) $clientid));
        return $this->db->fetchAll($result);
    }

    function getClientEmail($email) {
        $sql = "SELECT id FROM clients WHERE email='%s'";
        $result = $this->db->querySafe($sql, array($email));
        return ($result) ? $this->db->numRows($result) : NULL;
    }

    function saveClient($data, $clientid = NULL) {
        $isNew = $clientid === NULL;
        if ($isNew) {
            $salt = substr(str_replace('+', '.', base64_encode(sha1(microtime(true), true))), 0, 22);
            $hash = crypt($data['pass'], '$2a$12$' . $salt);
            $sql = "INSERT INTO clients (email, password, registerdate) VALUES ('%s', '%s', NOW())";
            $this->db->querySafe($sql, array($data["email"], $hash));
        } else {
            $salt = substr(str_replace('+', '.', base64_encode(sha1(microtime(true), true))), 0, 22);
            $hash = crypt($data['pass'], '$2a$12$' . $salt);
            $sql = "UPDATE clients SET password='%s' WHERE id='%d'";
            $this->db->querySafe($sql, array($hash, (int)$clientid));
        }

    }

    function authenticate($email, $pass) {
        $sql = "SELECT password FROM clients WHERE email='%s'";
        $result = $this->db->querySafe($sql, array($email));
        if (!$this->db->numRows($result)) {
            return NULL;
        }
        $hash = $this->db->fetchSingle($result);
        $sql = "SELECT id FROM clients WHERE email='%s' AND password='%s'";
        $result = $this->db->querySafe($sql, array($email, crypt($pass, $hash['password'])));
        $result = $this->db->fetchSingle($result);
        if ($result) {
            return $result['id'];
        } else {
            return false;
        }
    }

    function setLastVisit($clientid) {
        $sql = "UPDATE clients SET lastvisit=NOW() WHERE id='%d'";
        $this->db->querySafe($sql, array((int) $clientid));
    }


    function getCurrentClient() {
        return $this->session->get('client');
    }

}
