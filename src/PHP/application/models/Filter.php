<?php

class Filter extends Model {

    public function getClientFilters($client) {
        if ($res = $this->db->querySafe("SELECT * FROM `filters` WHERE `client`='%d'", array((int) $client))) {
            return $this->db->fetchAll($res);
        }
    }

    public function getClientFiltersConditions($client) {
        if ($res = $this->db->querySafe("SELECT `a`.`id` AS `filter`, `a`.`client`, `a`.`source`, `b`.`type`, `b`.`term` FROM `filters` AS `a` LEFT JOIN `filters_conditions` AS `b` ON `a`.`id` = `b`.`filter` WHERE `a`.`client`='%d' GROUP BY `a`.`id`", array($client))) {
            $filters = $this->db->fetchAll($res);
            //var_dump($filters);
            $restructuredFilters = array();
            foreach ($filters as $filter) {
                $restructuredFilters[$filter['source']][] = array(
                    'type' => $filter['type'],
                    'term' => $filter['term']
                );
            }
            return $restructuredFilters;
        }
    }

    public function getClientFiltersList($client) {
        if ($res = $this->db->querySafe("SELECT `a`.*, `b`.`title` AS `source_title`, `b`.`url` AS `source_url` FROM `filters` AS `a` LEFT JOIN `sources` AS `b` ON `a`.`source`=`b`.`id` WHERE `a`.`client`='%d'", array((int) $client))) {
            return $this->db->fetchAll($res);
        }
    }

    public function getFilterConditions($id) {
        if ($res = $this->db->querySafe("SELECT * FROM `filters_conditions` WHERE `id`='%d'", array((int) $id))) {
            return $this->db->fetchAll($res);
        }
    }

    public function addClientFilter($clientId, $sourceId, $filters) {
        if ($this->filterExists($clientId, $sourceId)) {
            return false;
        }

        if ($this->db->querySafe("INSERT INTO `filters` (`client`, `source`) VALUES ('%d', '%d')", array($clientId, $sourceId))) {
            $filterId = $this->db->insertId();
            foreach($filters as $filter) {
                $this->db->querySafe("INSERT INTO `filters_conditions` (`filter`, `type`, `term`) VALUES ('%d', '%d', '%s')", array($filterId, $filter['type'], $filter['term']));
            }
            return true;
        } else {
            return false;
        }
    }

    public function removeClientFilter($filterId) {
        $sql1 = "DELETE FROM `filters` WHERE `id`='%d'";
        $sql2 = "DELETE FROM `filters_conditions` WHERE `filter`='%d'";
        return $this->db->querySafe($sql1, array($filterId))
            && $this->db->querySafe($sql2, array($filterId));
    }

    public function filterExists($clientId, $sourceId) {
        return $this->db->numRows($this->db->querySafe("SELECT `id` FROM `filters` WHERE `client`='%d' AND `source`='%d'", array((int) $clientId, (int) $sourceId))) > 0;
    }
}
