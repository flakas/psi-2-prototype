<?php

class News extends Model {

    public function getNewsByGuidAndSource($guid, $sourceId) {
        return $this->db->fetchSingle($this->db->querySafe("SELECT * FROM news WHERE guid='%s' AND sourceid='%d'", array($guid, $sourceId)));
    }

    public function saveNews($news) {
        $sql = "INSERT INTO news (title, text, date, guid, sourceid, full_url) VALUES ";
        foreach ($news as $key=>$item) {
            $sql .= "('".$this->db->escape($item->title)."','".
                         $this->db->escape($item->text)."','".
                         $this->db->escape($item->date)."','".
                         $this->db->escape($item->guid)."','".
                         $this->db->escape($item->sourceid)."','".
                         $this->db->escape($item->full_url).
                     "')";
            if ($key+1 < count($news)) {
                $sql .= ", ";
            }
        }
        $this->db->query($sql);
    }

    public function getAllClientNews($client) {
        return $this->db->fetchAll($this->db->querySafe("SELECT * FROM news WHERE `sourceid` IN (SELECT source FROM client_sources WHERE client='%d')", array($client)));
    }

    public function getAllFilteredClientNews($client, $filters) {
        $filterCondition = array();
        if (!empty($filters)) {
            foreach ($filters as $source => $conditions) {
                $conditionText = "sourceid='" . $source ."'";
                foreach ($conditions as $condition) {
                    switch($condition['type']) {
                        case 1: // in title
                            $conditionText .= " AND `title` LIKE '%" . $condition['term'] . "%'";
                            break;
                        case 2: // not in title
                            $conditionText .= " AND `title` NOT LIKE '%" . $condition['term'] . "%'";
                            break;
                        case 3: // in description
                            $conditionText .= " AND `text` LIKE '%" . $condition['term'] . "%'";
                            break;
                        case 4: // not in description
                            $conditionText .= " AND `text` NOT LIKE '%" . $condition['term'] . "%'";
                            break;
                    }
                }
                $filterCondition[] = '(' . $conditionText . ')';
            }
        }
        if (!empty($filterCondition)) {
            $filterCondition = ' OR (' . implode(" OR ", $filterCondition) . ')';
        } else {
            $filterCondition = '';
        }

        $query = "SELECT * FROM news WHERE `sourceid` IN (SELECT source FROM client_sources WHERE client='%d' AND source NOT IN (SELECT source FROM filters WHERE client='%d'))%s ORDER BY date DESC";
        //echo vsprintf($query, array($client, $client, $filterCondition));
        return $this->db->fetchAll($this->db->querySafe($query, array($client, $client, $filterCondition)));
    }

}
