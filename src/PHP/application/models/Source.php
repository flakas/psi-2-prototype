<?php

class Source extends Model {

    public function getAllSources($start = 0, $limit = 20) {
        return $this->db->fetchAll($this->db->query("SELECT * FROM sources ORDER BY id ASC"));
    }

    public function getSourceById($id) {
        return $this->db->fetchSingle($this->db->querySafe("SELECT * FROM sources WHERE id='%d'", array((int) $id)));
    }

    public function getSourceByUrl($url) {
        return $this->db->fetchSingle($this->db->querySafe("SELECT * FROM sources WHERE url='%s'", array($url)));
    }

    public function getClientSources($client) {
        return $this->db->fetchAll($this->db->querySafe("SELECT b.* FROM client_sources AS a LEFT JOIN sources AS b ON a.source = b.id WHERE a.client='%d' ORDER BY b.id ASC", array((int) $client)));
    }

    /**
     * Sources that do not have any filters applied
     */
    public function getAvailableClientSources($client) {
        return $this->db->fetchAll($this->db->querySafe("SELECT b.* FROM client_sources AS a LEFT JOIN sources AS b ON a.source = b.id WHERE a.client='%d' AND b.id NOT IN (SELECT source FROM filters WHERE client='%d')", array($client, $client)));
    }

    public function addClientSource($client, $url) {
        $this->addSource($url);
        if (!$this->clientSubscribedTo($client, $url)) {
            $source = $this->getSourceByUrl($url);
            if ($this->db->querySafe("INSERT INTO client_sources (`client`, `source`) VALUES ('%d', '%d')", array((int) $client, (int) $source['id']))) {
                return true;
            }
        }
        return false;
    }

    public function addSource($url) {
        if (!$this->sourceExists($url)) {
            if ($this->db->querySafe("INSERT INTO sources (`url`) VALUES ('%s')", array($url))) {
                return true;
            }
        }
        return false;
    }

    public function removeClientSource($client, $id) {
        $source = $this->getSourceById($id);
        if ($source && $client) {
            if ($this->db->querySafe("DELETE FROM client_sources WHERE `client`='%d' AND `source`='%d'", array((int) $client, (int) $source['id']))) {
                return true;
            }
        }
        return false;
    }

    public function sourceExists($url) {
        return $this->db->numRows($this->db->querySafe("SELECT id FROM sources WHERE url='%s'", array($url))) > 0;
    }

    public function clientSubscribedTo($client, $url) {
        return $res = $this->db->querySafe("SELECT b.id FROM client_sources AS a RIGHT JOIN sources AS b ON a.source = b.id WHERE a.url='%s' AND b.client = '%d' ", array($url, (int) $client)) && $this->db->numRows($res) > 0;
    }

    public function setSourceLastCheck($sourceid) {
        $this->db->querySafe("UPDATE sources SET lastcheck=now() WHERE id='%d'", array($sourceid));
    }

    public function increaseInactivityTimes($sourceid) {
        $result = $this->db->querySafe("SELECT num_inactive FROM sources WHERE id='%d'", array($sourceid));
        $num = $this->db->fetchSingle($result);
        $this->db->querySafe("UPDATE sources SET num_inactive='%d' WHERE id='%d'", array((int)$num['num_inactive']+1, $sourceid));
    }

    public function setSourceTitle($sourceid, $title) {
        $this->db->querySafe("UPDATE sources SET title='%s' WHERE id='%d'", array($title, $sourceid));
    }

    public function getPopularSources($clientid, $limit) {
        $sql = "SELECT s.*, COUNT(c.client) AS number 
                FROM sources AS s 
                LEFT JOIN client_sources AS c 
                    ON (s.id = c.source) 
                WHERE s.title IS NOT NULL 
                AND c.source NOT IN 
                    (SELECT source FROM client_sources 
                     WHERE client=%d) 
                GROUP BY c.source 
                ORDER BY number DESC 
                LIMIT %d";
        $result = $this->db->querySafe($sql, array((int)$clientid, (int)$limit));
        return $this->db->fetchAll($result); 
    }

}
