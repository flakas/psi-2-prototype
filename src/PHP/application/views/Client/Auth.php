<?php if ($mode == "loginform"): ?>
    <form action="/Client" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Prisijungti</h2>
        <input type="text" class="input-block-level" placeholder="El. pašto adresas" name="email" />
        <input type="password" class="input-block-level" placeholder="Slaptažodis" name="pass" />
        <button class="btn btn-large btn-primary" type="submit">Prisijungti</button>
        <input type="hidden" name="task" value="login"/><br />
        <a href="/Client/Register">Registruotis</a>
    </form>
<?php elseif ($mode == "registerform"): ?>
    <form action="/Client/Register" method="POST" class="form-signin">
        <h2 class="form-signin-heading">Registruokitės</h2>
        <input type="text" class="input-block-level" placeholder="El. pašto adresas" name="email" />
        <input type="password" class="input-block-level" placeholder="Slaptažodis" name="pass" />
        <input type="password" class="input-block-level" placeholder="Pakartokite slaptažodį" name="pass-repeat" />
        <button class="btn btn-large btn-primary" type="submit">Užsiregistruoti</button>
        <input type="hidden" name="task" value="register"/><br />
        <a href="/Client">Prisijungti</a>
    </form>
<?php endif; ?>
