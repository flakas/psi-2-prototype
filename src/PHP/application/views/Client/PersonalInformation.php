<?php if (isset($message)): ?>
    <p><strong><?php echo $message; ?></strong></p>
<?php endif; ?>


    <form action="/Client/personalInfo" method="POST" class="form-horizontal">
        <div class="control-group">
            <label for="pass" class="control-label">Naujas slaptažodis</label>
            <div class="controls">
                <input type="password" name="pass" value="" />
            </div>
        </div>
        <div class="control-group">
            <label for="pass-2" class="control-label">Pakartokite naują slaptažodį</label>
            <div class="controls">
                <input type="password" name="pass-2" value="" />
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <input type="submit" value="Atnaujinti" class="btn" />
            </div>
        </div>
        <input type="hidden" name="update" value="1" />
    </form>
