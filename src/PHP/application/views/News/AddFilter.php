<h2>Pridėti filtrą</h2>
<?php
if (isset($response) && $response['success']) {
    echo 'Jūs sėkmingai pridėjote filtrą. ';
} else {
    $sourcesOptions = '';
    if (!empty($sources)) {
        foreach ($sources as $source) {
            $sourceName = (empty($source['title'])) ? '' : $source['title'] . ' - ';
            $sourceName .= $source['url'];
            $sourcesOptions .= sprintf('<option value="%d">%s</option>', $source['id'], $sourceName);
        }
    }
?>
<form method="post" name="addfilter" action="">
<table class="table">
    <tr>
        <td>
            <label for="source">Šaltinis</label>
        </td>
        <td>
            <select name="source">
                <?php echo $sourcesOptions; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <label for="filter_type">Filtro tipas</label>
        </td>
        <td>
            <select name="filter_type">
                <option value="1">Yra antraštėje</option>
                <option value="2">Nėra antraštėje</option>
                <option value="3">Yra turinyje</option>
                <option value="4">Nėra turinyje</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <label for="term">Raktažodis</label>
        </td>
        <td>
            <input type="text" name="term" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <input type="submit" name="submit" value="Pridėti filtrą" />
        </td>
    </tr>
</table>
</form>
<?php
}
