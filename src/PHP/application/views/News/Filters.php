<h2>Turinio filtrai</h2>
<?php
if (count($filters)) {
?>
<table class="table">
    <tr>
        <th>Nr</th>
        <th>Taikomas šaltiniui</th>
    </tr>
<?php
    foreach ($filters as $filter) {
        echo sprintf('<tr><td>%d</td><td>%s</td></tr>', $filter['id'], $filter['source_url']);
    }
?>
</table>
<?php
}
