<?php foreach ($news as $item): ?>
    <article id="news">
        <h3>
            <?php if (isset($item['full_url']) && !empty($item['full_url'])): ?>
                <a href="<?php echo $item['full_url']; ?>" target="_blank">
            <?php endif; ?>
            <?php echo $item['title']; ?>
            <?php if (isset($item['full_url']) && !empty($item['full_url'])): ?>
                </a>
            <?php endif; ?>
        </h3>
        <p class="time"><time><?php echo $item['date']; ?></time></p>
        <?php echo $item['text']; ?>
        <hr />
    </article>
<?php endforeach; ?>

<?php if (isset($popular) && count($popular)): ?>
<section class="popular">
    <h3>Siūlome užsiprenumeruoti šiuos populiarius šaltinius</h3>
    <table>
        <thead>
            <tr>
                <th>Šaltinio pavadinimas</th>
                <th>Šaltinio adresas</th>
                <th>Užsiprenumeravę</th>
                <th>Pridėti šaltinį</th>
            </tr>
        </thead>
        <tbody>
    <?php foreach ($popular as $item): ?>
           <tr>
                <td><?php echo ($item['title'] ? $item['title'] : ""); ?> </td>
                <td><?php echo $item['url']; ?> </td>
                <td><?php echo $item['number']; ?> </td>
                <td>
                    <form action="/news/addSource" method="post">
                        <input type="submit" name="submit" value="Pridėti šaltinį" />
                        <input type="hidden" name="url" id="url" value="<?php echo $item['url']; ?>" />
                    </form>
                </td>


           </tr>
    <?php endforeach; ?>
        </tbody>
    </table>
</section>
<?php endif; ?>
