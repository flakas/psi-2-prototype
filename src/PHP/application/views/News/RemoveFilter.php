<h2>Pašalinti filtrą</h2>
<?php
if (isset($response) && $response['success']) {
    echo 'Filtras sėkmingai pašalintas';
} else {
    if (!isset($filters) || empty($filters)) {
        echo 'Jūs nesate nustatę filtrų';
    } else {
        $options = '';
        foreach ($filters as $filter) {
            $name = (empty($filter['source_title'])) ? '' : $filter['source_title'] . ' - ';
            $name .= $filter['source_url'];
            $options .= sprintf('<option value="%d">%s</option>', $filter['id'], $name);
        }

?>
<form method="post" name="removesource" action="">
    <table class="table">
        <tr>
            <td><label for="filter_id">Filtras: </label></td>
            <td><select name="filter_id"><?php echo $options; ?></select></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Pašalinti filtrą" /></td>
        </tr>
    </table>
</form>
<?
    }
}
