<h2>Atsisakyti šaltinio prenumeratos</h2>
<?php
if (isset($response) && $response['success']) {
    echo 'Šaltinio prenumeratos sėkmingai atsisakyta';
} else {

    $options = '';
    foreach ($sources as $source) {
        $name = (empty($source['title'])) ? '' : $source['title'] . ' - ';
        $name .= $source['url'];
        $options .= sprintf('<option value="%d">%s</option>', $source['id'], $name);
    }

?>
<form method="post" name="removesource" action="">
    <table class="table">
        <tr>
            <td><label for="source_id">Šaltinis: </label></td>
            <td><select name="source_id"><?php echo $options; ?></select></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" name="submit" value="Atsisakyti šaltinio prenumeratos" /></td>
        </tr>
    </table>
</form>
<?
}
