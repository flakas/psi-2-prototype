<h2>Jūsų prenumeruojami šaltiniai</h2>
<?php
if (count($sources)):
?>
<table class="table">
    <tr>
        <th>Nr</th>
        <th>Adresas</th>
    </tr>
<?php
    foreach($sources as $source) {
        echo '<tr><td>' . $source['id'] . '</td><td>' . $source['url'] . '</td></tr>';
    }
endif;
?>
</table>
