<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8">
        <title>ARTA</title>
        <link rel="stylesheet" type="text/css" href="/style/main.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" media="screen" />
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>
        <link rel="stylesheet" type="text/css" href="/style/bootstrap-responsive.min.css" media="screen" />
    </head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <a class="brand" href="#">ARTA</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li><a href="/">Naujienos</a></li>
              <li><a href="/news/sources">Šaltiniai</a></li>
              <li><a href="/news/addSource">Pridėti šaltinį</a></li>
              <li><a href="/news/removeSource">Pašalinti šaltinį</a></li>
              <li><a href="/news/filters">Filtrai</a></li>
              <li><a href="/news/addFilter">Pridėti filtrą</a></li>
              <li><a href="/news/removeFilter">Pašalinti filtrą</a></li>
              <li><a href="/update" target="_blank">Atnaujinti šaltinius</a></li>
              <li><a href="/client/personalInfo">Keisti slaptažodį</a></li>
              <li><a href="/client/logout">Atsijungti</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>


    <div class="container">
        <?php if (isset($message) && !empty($message)): ?>
            <div class="alert">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>

        <?php echo $body; ?>

    </div> <!-- /container -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("article#news a").attr("target","_blank");
        });
    </script>
</body>
</html>
