<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>ARTA</title>
<link rel="stylesheet" type="text/css" href="/style/main.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/style/bootstrap.min.css" media="screen" />
<style>
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
</style>
<link rel="stylesheet" type="text/css" href="/style/bootstrap-responsive.min.css" media="screen" />
</head>
<body>
    <div class="container">

        <?php if (isset($message) && !empty($message)): ?>
            <div class="alert">
                <?php echo $message; ?>
            </div>
        <?php endif; ?>
        <?php echo $body; ?>

    </div> <!-- /container -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js" type="text/javascript"></script>
    <script src="/js/bootstrap.min.js"></script>
</body>
</html>
