<?php

function get_instance() {
    return Core::getInstance();
}

// Include base components
require(SYSTEMPATH . 'Controller.php');
require(SYSTEMPATH . 'Model.php');
