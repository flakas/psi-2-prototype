<?php

class Config {

    private $core;
    private $configs;

    public function __construct() {
        $this->core = get_instance();
        $this->configs = array();
        $this->load();
    }

    public function item($key) {
        if (isset($this->configs[$key])) {
            return $this->configs[$key];
        } else {
            throw new Exception ("Such config does not exist: " . $key);
        }
    }

    private function load() {
        $path = APPPATH. 'config/config.php';
        if (file_exists($path)) {
            require($path);
            $this->configs = $config;
        }
    }
}
