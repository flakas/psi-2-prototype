<?php

abstract class Controller {

    private $core;

    public abstract function index();

    public function __get($key) {
        if (!$this->core) {
            $this->core =& get_instance();
        }
        return $this->core->$key;
    }
}
