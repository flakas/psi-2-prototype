<?php

class DB {

    private $core;
    private $conn;

    public function __construct($host, $user, $password, $db) {
        $this->core = get_instance();
        $this->connect($host, $user, $password, $db);
    }

    private function connect($host, $user, $password, $db) {
        $this->conn = mysqli_connect($host, $user, $password, $db);
    }

    public function query($query) {
        return mysqli_query($this->conn, $query);
    }

    public function querySafe($query, $values) {
        array_walk($values, array($this, 'escape'));
        return $this->query(vsprintf($query, $values));
    }

    public function numRows($res) {
        return mysqli_num_rows($res);
    }

    public function fetchAll($res) {
        if ($res == NULL) return NULL;
        $return = array();
        while ($row = mysqli_fetch_assoc($res)) {
            $return[] = $row;
        }
        return $return;
    }

    public function fetchSingle($res) {
        return ($res != NULL) ? mysqli_fetch_assoc($res) : NULL;
    }

    public function error() {
        return mysqli_error($this->conn);
    }

    public function escape($text) {
        return mysqli_real_escape_string($this->conn, $text);
    }

    public function insertId() {
        return mysqli_insert_id($this->conn);
    }

}
