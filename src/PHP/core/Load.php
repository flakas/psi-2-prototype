<?php

class Load {

    private $core;
    private $_controllers;
    private $_models;
    private $_views;
    private $_libraries;

    public function __construct() {
        $this->_controllers = array();
        $this->_models = array();
        $this->_views = array();
        $this->_libraries = array();
    }

    public function initialize() {
        $this->autoload();
    }

    public function model($model) {
        $model = ucfirst(strtolower($model));
        if (in_array($model, array_keys($this->_models))) {
            return $this->getCore()->$model;
        }

        if (file_exists('application/models/' . $model . '.php')) {
            require('application/models/' . $model . '.php');
            $this->getCore()->$model = new $model();
            $this->_models[$model] = true;
            return $this->getCore()->$model;
        } else {
            throw new Exception("No such model: " . $model);
        }
    }

    public function view($view, $data=array(), $returnString=false) {
        if (file_exists('application/views/' . $view . '.php')) {
            $data['this'] = $this->core;
            extract($data);
            ob_start();
            require('application/views/' . $view . '.php');
            if ($returnString) {
                $buffer = ob_get_contents();
                @ob_end_clean();
                return $buffer;
            }
        } else {
            throw new Exception("No such view: " . $view);
        }

    }

    public function controller($controller) {
        $controller = ucfirst(strtolower($controller));
        if (in_array($controller, array_keys($this->_controllers))) {
            return $this->getCore()->controllers->$controller;
        }

        if (file_exists('application/controllers/' . $controller . '.php')) {
            include('application/controllers/' . $controller . '.php');
            $className = $controller . '_controller';
            $this->getCore()->controllers->$controller = new $className();
            $this->_controllers[$controller] = true;
            return $this->getCore()->controllers->$controller;
        } else {
            throw new Exception("No such controller: " . $controller);
        }
    }

    public function library($library) {
        $library = ucfirst(strtolower($library));
        if (in_array($library, array_keys($this->_libraries))) {
            return $this->getCore()->libraries->$library;
        }

        if (file_exists('application/libraries/' . $library . '.php')) {
            include('application/libraries/' . $library . '.php');
            $this->getCore()->libraries->$library = new $library();
            $this->_libraries[$library] = true;
            return $this->getCore()->libraries->$library;
        } else {
            throw new Exception("No such library: " . $library);
        }
    }


    private function autoload() {
        require(SYSTEMPATH . 'Router.php');
        $this->getCore()->router = new Router();
        require(SYSTEMPATH . 'Config.php');
        $this->getCore()->config = new Config();
        $this->db();
        $this->session();
        $this->getCore()->template = $this->library('Template');
    }

    private function db() {
        require(SYSTEMPATH . 'DB.php');
        $db = $this->getCore()->config->item('db');
        $this->getCore()->db = new DB($db['host'], $db['username'], $db['password'], $db['database']);
    }

    private function session() {
        require(SYSTEMPATH . 'Session.php');
        $this->getCore()->session = new Session();
    }

    private function getCore() {
        if (!$this->core) {
            $this->core = get_instance();
        }
        return $this->core;
    }
}
