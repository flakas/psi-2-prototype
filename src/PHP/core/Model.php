<?php

abstract class Model {

    private $core;

    public function __get($key) {
        if (!$this->core) {
            $this->core =& get_instance();
        }
        return $this->core->$key;
    }
}
