<?php

class Router {

    private $core;
    private $routes;

    public function __construct() {
        $this->core = get_instance();
        $this->autoLoad();
    }

    public function route() {
        $request = $this->parseRequest();
        $controller = ($request['controller']) ? $request['controller'] : $this->routes['default_controller'];
        $controller = ucfirst(strtolower($controller));
        $function = ($request['function']) ? $request['function'] : 'index';
        try {
            $this->load->controller($controller);
        } catch (Exception $e) {
            $this->displayError(404);
            return;
        }

        if (method_exists($this->controllers->$controller, $function)) {
            call_user_func_array(array($this->controllers->$controller, $function), $request['params']);
        } else {
            $this->displayError(404);
        }
    }

    private function parseRequest() {
        $request = $_SERVER['REQUEST_URI'];

        if ($request[0] == '/') {
            $request = substr($request, 1);
        }
        if (strpos($request, 'index.php') === 0) {
            $request = substr($request, strlen('index.php'));
        }
        if ($request[0] == '/') {
            $request = substr($request, 1);
        }

        $request = explode('/', $request);

        $processedRequest = array(
            'controller' => '',
            'function' => '',
            'params' => array()
        );

        $totalTokens = count($request);
        if ($totalTokens > 0) { // Controller
            $processedRequest['controller'] = $request[0];
        }

        if ($totalTokens > 1) { // Function
            $processedRequest['function'] = $request[1];
        }

        if ($totalTokens > 2) { // Function params
            $processedRequest['params'] = array_slice($request, 2);
        }

        return $processedRequest;
    }

    private function autoLoad() {
        $path = APPPATH . 'config/routes.php';
        if (file_exists($path)) {
            require($path);
            $this->routes = $routes;
        }
    }

    private function displayError($error) {
        switch ($error) {
            case 404:
                require(APPPATH . 'views/' . $this->routes['error_404'] . '.php');
                break;
            default:
                require(APPPATH . 'views/' . $this->routes['error_500'] . '.php');
                break;
        }
    }

    public function __get($key) {
        return $this->core->$key;
    }

}
