<?php

class Session {

    private $core;

    public function __construct() {
        if (session_id()) {
            session_unset();
            session_destroy();
        }

        $this->core = get_instance();
        $this->start();
    }

    private function start() {

        session_start();
    }

    public function set($name, $value = null) {
        if (null === $value) {  /* unset session  var*/
            unset($_SESSION[$name]); 
        }
        else
        {
            $_SESSION[$name] = $value;
        }
    }

    public function get($name) {
        return isset($_SESSION[$name]) ? $_SESSION[$name] : null;
    }

    public function destroy() {
        session_unset();
        session_destroy();
    }
}
