DROP TABLE IF EXISTS client_sources;
CREATE TABLE client_sources (
    client INT NOT NULL,
    source INT NOT NULL,
    PRIMARY KEY(client, source),
    FOREIGN KEY (client) REFERENCES clients(id) ON DELETE CASCADE,
    FOREIGN KEY (source) REFERENCES sources(id) ON DELETE CASCADE 
) ENGINE=MyISAM;

DROP TABLE IF EXISTS filters;
CREATE TABLE filters (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    client INT NOT NULL,
    source INT NOT NULL,
    FOREIGN KEY (client) REFERENCES clients(id) ON DELETE CASCADE,
    FOREIGN KEY (source) REFERENCES sources(id) ON DELETE CASCADE 
) ENGINE=MyISAM;

DROP TABLE IF EXISTS filters_conditions;
CREATE TABLE filters_conditions (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    filter INT NOT NULL,
    type INT NOT NULL,
    term VARCHAR(255) NOT NULL,
    INDEX filter (`filter`),
    FOREIGN KEY (filter) REFERENCES filters(id) ON DELETE CASCADE
) ENGINE=MyISAM;

DROP TABLE IF EXISTS news;
CREATE TABLE news (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title           VARCHAR(300),
    text            TEXT,
    date            DATETIME,
    filterid        INT,
    guid            VARCHAR(2083),
    sourceid        INT,
    full_url             VARCHAR(2083)
) ENGINE=MyISAM;

DROP TABLE IF EXISTS sources;
CREATE TABLE sources (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    url             VARCHAR(2083) NOT NULL,
    title           VARCHAR(300),
    lastcheck       DATETIME,
    num_inactive    INT DEFAULT 0
) ENGINE=MyISAM;

DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
    id              INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    email           VARCHAR(100) NOT NULL,
    password        CHAR(100),
    lastvisit       DATETIME,
    registerdate    DATETIME
) ENGINE=MyISAM;
